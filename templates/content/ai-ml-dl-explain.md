# Differences between AI, ML, DL, and What They Are

> This was originally published on Medium.

When you hear something with “AI”, what comes to mind? Although most are referring to deep learning, a new technique to train algorithms to make decisions (e.g. is this a cat or a dog?), AI is a broad field, and there is much more to it. Let’s explore the what the differences and similarities between AI (**a**rtificial **i**ntelligence), ML (**m**achine **l**earning), DL (**d**eep **l**earning), and what they are.

### AI

![](https://cdn-images-1.medium.com/max/1791/1*yh1J9UOzj9926_AcNi_NEw.png)

A diagram showing some subfields of AI. By Ken Shibata.

AI is a _really_ broad field. The diagram below shows some, but many others are hidden for the sake of simplicity. The “AI” that you might hear or see on the Internet is probably referring to deep learning, but there are many other interesting subfields of AI which you should definitely check out.

AI can be used in voice assistants, robotics, and generate terrain. For example, Minecraft uses an AI (procedural generation) to generate the “infinite” terrain that can be experienced in the game. Another example is in robotics, where robots can automatically avoid objects using sensors and an AI.

### Machine Learning

Machine learning is basically all about changing algorithms to adapt to do tasks better. This doesn’t have to be artificial neural networks (the ones with the complicated diagrams). It can simply be a manually written algorithm which parses data, learn from the data, and make decisions based on input and the knowledge gained from learning.

Machine learning can be applied to applications such as non neural networks, like predicting a graph (linear regression). But, deep learning, which is “in” ML, can be used to detect objects, differentiate between robots and humans viewing a webpage (e.g. reCAPTCHA by Google), and differentiate two humans faces.

### Deep Learning

Deep learning is a subset of machine learning, a subset of AI. Deep learning is usually the “AI” you’ll probably hear about. This is about making a mathematical representation of a neural network, and a neural network in not like a biological neural network.

What’s the difference between a biological one (in a human) and ones used in deep learning? A biological neural network is a collection of neurons, connected together. This is what a biologist might think of. A biophysicist might think of a more abstract neural network, although similar to a biological one. A computer scientist might think of a neural network that is made purely of math (e.g. not including physics).

Each “neuron” is a function (e.g.`f(x)`). Usually, when calculating results, libraries such as TensorFlow or PyTorch use matrices to simplify this. A given neural network might be able to be represented as layers of matrices, where each layer is the results of the neuron functions being applied on. Also, when there are more than two layers (i.e. the “input” and “output” layers), the network is called a “deep neural network”, and the layers in between are called “hidden” layers.

```
Input:  [0,   1,   2,   3]Hidden: [?,   ?,   ?,   ?]Output: [0.7, 0.1, 0.1, 0.1]
```

Deep learning is used to detect faces and differentiate them (facial recognition like on a phone), identify objects (used in self driving cars), and can also converse like a human (GPT-3).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg2ODQ0NzQyMV19
-->